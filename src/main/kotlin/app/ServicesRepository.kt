package ru.hse.cs.cloud.app

import org.springframework.data.repository.CrudRepository

interface ServicesRepository : CrudRepository<ServiceEntity, String>