package ru.hse.cs.cloud.app

import org.springframework.stereotype.Component
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy


@Component
class ConstructAndDestroyApp(val repository: ServicesRepository) {
    val myIp = System.getenv("IP")

    @PostConstruct
    fun addServiceItem() {
        if (repository.findById(myIp).isPresent) {
            repository.deleteById(myIp)
        }
        repository.save(ServiceEntity(myIp, true)) // my server is available
    }

    @PreDestroy
    fun removeServiceItem() {
        if (repository.findById(myIp).isPresent) {
            repository.deleteById(myIp)
        }
        repository.save(ServiceEntity(myIp, false)) // my server shuts down
    }
}