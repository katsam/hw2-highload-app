package ru.hse.cs.cloud.app

import javax.persistence.*

@Entity
@Table(name = "services")
class ServiceEntity(
    @Id var ip: String,
    var status: Boolean
)