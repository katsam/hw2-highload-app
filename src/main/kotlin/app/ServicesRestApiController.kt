package ru.hse.cs.cloud.app

import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@Component
class ServicesRestApiController(val repository: ServicesRepository) {
    val myIp = System.getenv("IP")

    @GetMapping("/healthcheck")
    fun healthCheck(): AvailableServices {
        println("get ip = $myIp")
        val servicesList = repository.findAll()
            .filter { it.status }
            .map { ServiceStatus(it.ip, "AVAILABLE") }

        return AvailableServices(myIp, servicesList)
    }
}

data class ServiceStatus(val ip: String, val status: String)
data class AvailableServices(val ip: String, val services: List<ServiceStatus>)