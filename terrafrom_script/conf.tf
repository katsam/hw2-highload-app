terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "~> 0.49.0"
    }
  }
}

provider "yandex" {
  token                    = "OAtoken"
  cloud_id                 = "b1gmegtvd8j3h00urc3n"
  folder_id                = "b1gckjfh69kc94a5km3e"
  zone                     = "ru-central1-a"
}

resource "yandex_vpc_network" "hw2_network" {
  name = "hw2-network"
  description = "net for highload app (hw2)"
}

resource "yandex_vpc_subnet" "hw2_public_subnet" {
  name = "public-subnet"
  v4_cidr_blocks = ["10.0.0.0/24"]
  network_id     = yandex_vpc_network.hw2_network.id
}

resource "yandex_compute_instance" "nat" {
  boot_disk {
    initialize_params {
      image_id = "fd85tqltvlg3mtufp0il"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.hw2_public_subnet.id
    nat       = true
  }
  metadata = {
    serial-port-enable : "0"
    user-data = file("cloud_config.yml")
    ssh-keys  = "katsam:ssh"
  }
  resources {
    cores         = 2
    memory        = 1
    core_fraction = 5
  }
  scheduling_policy {
    preemptible = true
  }
  name               = "nat"
  hostname           = "nat"
  service_account_id = "ajedub2p5u0tlbsoe00m"
  platform_id        = "standard-v2"
}

resource "yandex_vpc_route_table" "route_table" {
  network_id = yandex_vpc_network.hw2_network.id
  static_route {
    destination_prefix = "0.0.0.0/0"
    next_hop_address   = yandex_compute_instance.nat.network_interface.0.ip_address
  }
}

resource "yandex_vpc_subnet" "hw2_private_subnet" {
  name           = "subnet-private"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.hw2_network.id
  v4_cidr_blocks = ["10.0.1.0/24"]
  route_table_id = yandex_vpc_route_table.route_table.id
}

data "yandex_compute_image" "container-optimized-image" {
  family = "container-optimized-image"
}

resource "yandex_compute_instance" "hw2_highload_app_database" {
  service_account_id = "aje"
  name               = "hw2-highload-app-db"
  hostname           = "hw2-highload-app-db"
  platform_id        = "standard-v2"

  resources {
    cores  = 2
    memory = 2
    core_fraction = 5
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.container-optimized-image.id
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.hw2_private_subnet.id
  }

  scheduling_policy {
    preemptible = true
  }

  metadata = {
    docker-container-declaration = file("postgres.yml")
    user-data                    = file("cloud_config.yml")
    ssh-keys                     = "katsam:ssh"
  }
}

resource "yandex_compute_instance" "vm_1" {
  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.container-optimized-image.id
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.hw2_private_subnet.id
  }
  resources {
    cores         = 2
    memory        = 2
    core_fraction = 5
  }
  scheduling_policy {
    preemptible = true
  }
  metadata = {
    docker-container-declaration = templatefile("vm.yml",
    { DB_HOST = yandex_compute_instance.hw2_highload_app_database.network_interface.0.ip_address })
    user-data                    = file("cloud_config.yml")
    ssh-keys                     = "katsam:ssh"
  }
  name               = "hw2-highload-app-1"
  hostname           = "hw2-highload-app-1"
  service_account_id = "aje"
  platform_id        = "standard-v2"
}

resource "yandex_compute_instance" "vm_2" {
  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.container-optimized-image.id
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.hw2_private_subnet.id
  }
  resources {
    cores         = 2
    memory        = 2
    core_fraction = 5
  }
  scheduling_policy {
    preemptible = true
  }
  metadata = {
    docker-container-declaration = templatefile("vm.yml",
    { DB_HOST = yandex_compute_instance.hw2_highload_app_database.network_interface.0.ip_address })
    user-data                    = file("cloud_config.yml")
    ssh-keys                     = "katsam:ssh"
  }
  name               = "hw2-highload-app-2"
  hostname           = "hw2-highload-app-2"
  service_account_id = "aje"
  platform_id        = "standard-v2"
}

resource "yandex_compute_instance" "nginx" {
  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.container-optimized-image.id
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.hw2_public_subnet.id
    nat = true
  }
  resources {
    cores         = 2
    memory        = 2
    core_fraction = 5
  }
  scheduling_policy {
    preemptible = true
  }
  metadata = {
    docker-container-declaration = templatefile("nginx.yml", {
      VM1_ADDR = yandex_compute_instance.vm_1.network_interface.0.ip_address
      VM2_ADDR = yandex_compute_instance.vm_2.network_interface.0.ip_address
    })
    user-data                    = file("cloud_config_nginx.yml")
    ssh-keys                     = "katsam:ssh"
  }
  name               = "nginx"
  hostname           = "nginx"
  service_account_id = "aje"
  platform_id        = "standard-v2"
}

output "nginx_ip" {
  value = yandex_compute_instance.nginx.network_interface.0.nat_ip_address
}
output "postgres" {
  value = yandex_compute_instance.hw2_highload_app_database.network_interface.0.ip_address
}